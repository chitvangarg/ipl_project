const matchPlayedPerYear = require("./src/server/1-matches-per-year.cjs")
const matchesWonPerTeamPerYear = require("./src/server/2-matches-won-per-team-per-year.cjs")
const extraRunsConceededPerTeam = require("./src/server/3-extra_runs_conceeded_in_2016.cjs")
const economicalBowlerIn2015 = require("./src/server/4-10-economical-bowler.cjs")
const tossWonMatchWon = require("./src/server/5-toss-won-match-won.cjs")
const highestPlayerOfTheMatchPerSeason = require("./src/server/6-highest-award-each-season.cjs")
const strikerateForEachBatsmen = require("./src/server/7-strike-rate-for-batsmen-for-each-season.cjs")
const maxDismissalOfPlayer = require("./src/server/8-dissmissals-count.cjs")
const bestEconomyPlayerInSuperOver = require("./src/server/9-best-economy-super-over.cjs")

const matchPlayedPerYearJSON = "./src/public/output/1-matches-per-year.json"
const matchWonPerYearPerteamJson =
    "./src/public/output/2-matches-won-per-team-per-year.json"
const extraRunsConceededPerTeamJson =
    "./src/public/output/3-extra_runs_conceeded_in_2016.json"
const economicalBowlerIn2015Json =
    "./src/public/output/4-10-economical-bowler.json"
const tossWonMatchWonJson = "./src/public/output/5-toss-won-match-won.json"
const highestPlayerOfTheMatchPerSeasonJson =
    "./src/public/output/6-highest-award-each-season.json"
const strikerateForEachBatsmenJson =
    "./src/public/output/7-strike-rate-for-batsmen-for-each-season.json"
const maxDismissalOfPlayerJson = "./src/public/output/8-dissmissals-count.json"
const bestEconomyPlayerInSuperOverJson =
    "./src/public/output/9-best-economy-super-over.json"

const csv = require("csvtojson")
const fs = require("fs")

const dataFiles = ["./src/data/matches.csv", "./src/data/deliveries.csv"]

const writeFile = function (matchPlayedPerYearJSON, res) {
    fs.writeFileSync(
        matchPlayedPerYearJSON,
        JSON.stringify(res, null, 2),
        (err) => {
            if (err) {
                console.log("Error in conversion")
            } else {
                console.log("Successfully converted")
            }
        }
    )
}

Promise.all([csv().fromFile(dataFiles[0]), csv().fromFile(dataFiles[1])])
    .then(([matchesData, deliveriesData]) => {
        //1
        const matchPlayedPerYearRes = matchPlayedPerYear(matchesData)

        //2
        const matchesWonPerTeamPerYearRes =
            matchesWonPerTeamPerYear(matchesData)

        //3
        const extraRunsConceededPerTeamRes = extraRunsConceededPerTeam(
            matchesData,
            deliveriesData
        )

        //4
        const economicalBowlerIn2015Res = economicalBowlerIn2015(
            matchesData,
            deliveriesData
        )

        //5
        const tossWonMatchWonRes = tossWonMatchWon(matchesData)

        //6
        const highestPlayerOfTheMatchPerSeasonRes =
            highestPlayerOfTheMatchPerSeason(matchesData)

        // 7
        const strikerateForEachBatsmenRes = strikerateForEachBatsmen(
            matchesData,
            deliveriesData
        )

        // 8
        const maxDismissalOfPlayerRes = maxDismissalOfPlayer(deliveriesData)

        // 9
        const bestEconomyPlayerInSuperOverRes =
            bestEconomyPlayerInSuperOver(deliveriesData)

        writeFile(matchPlayedPerYearJSON, matchPlayedPerYearRes)
        writeFile(matchWonPerYearPerteamJson, matchesWonPerTeamPerYearRes)
        writeFile(extraRunsConceededPerTeamJson, extraRunsConceededPerTeamRes)
        writeFile(economicalBowlerIn2015Json, economicalBowlerIn2015Res)
        writeFile(tossWonMatchWonJson, tossWonMatchWonRes)
        writeFile(
            highestPlayerOfTheMatchPerSeasonJson,
            highestPlayerOfTheMatchPerSeasonRes
        )
        writeFile(strikerateForEachBatsmenJson, strikerateForEachBatsmenRes)
        writeFile(maxDismissalOfPlayerJson, maxDismissalOfPlayerRes)
        writeFile(
            bestEconomyPlayerInSuperOverJson,
            bestEconomyPlayerInSuperOverRes
        )
    })
    .catch((err) => {
        console.error("Error converting CSV to JSON:", err)
    })
