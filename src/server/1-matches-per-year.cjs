function matchPlayedPerYear(matches) {
    let years = matches.map((elem) => elem.season)

    // if resulting object has year value then add else assign it with 0
    let countPerSeason = years.reduce((accum, season) => {
        accum[season] = (accum[season] || 0) + 1
        return accum
    }, {})

    return countPerSeason
}

module.exports = matchPlayedPerYear
